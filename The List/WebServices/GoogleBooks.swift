//
//  GoogleBooks.swift
//  The List
//
//  Created by Orhun Dündar on 14.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import ObjectMapper
import SwiftyJSON

class GoogleBooksApi {
    
    static func getWith(text:String, success: @escaping ([BooksForGoogleApi]?) -> Void) {
        
        var bookList:[BooksForGoogleApi] = []
        // any search query....
        //spaces must be replaced by +...
        let searchText:String = text.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        let original = "https://www.googleapis.com/books/v1/volumes?q=" + searchText
        var url = URL(string: "")
        if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
            url = URL(string: encoded)
        }
        let session = URLSession(configuration: .default)
        session.dataTask(with: url!) { (data, _, err) in
            if err != nil {
                print((err?.localizedDescription)!)
                return
            }
            let json = try! JSON(data: data!)
            if let items = json["items"].array {
                for i in items{
                    let id = i["id"].stringValue
                    let title = i["volumeInfo"]["title"].stringValue
                    
                    var author = ""
                    if let authors = i["volumeInfo"]["authors"].array {
                        for j in authors{
                            author += "\(j.stringValue)"
                        }
                    }
                    
                    
                    let description = i["volumeInfo"]["description"].stringValue
                    let imurl = i["volumeInfo"]["imageLinks"]["thumbnail"].stringValue
                    let url1 = i["volumeInfo"]["previewLink"].stringValue
                    
                    bookList.append(BooksForGoogleApi(id: id, title: title, authors: author, desc: description, imurl: imurl, url: url1))
                }
                success(bookList)
            }
        }.resume()
    }
    
}
