//
//  ItemType.swift
//  The List
//
//  Created by Orhun Dündar on 13.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import Foundation

enum ItemType:Int {
    case Movie = 1
    case Book = 2
    case Experiance = 3
    
    static func getType(value:Int) -> ItemType {
        switch value {
        case 1:
            return Movie
        case 2:
            return Book
        case 3:
            return Experiance
        default:
            return Experiance
        }
    }
}
