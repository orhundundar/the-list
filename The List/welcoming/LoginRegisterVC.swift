//
//  LoginRegisterVC.swift
//  The List
//
//  Created by Orhun Dündar on 13.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import SwiftVideoBackground

enum Login_or_Register {
    case Login,Register
}

class LoginRegisterVC: UIViewController {
    
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginRegisterButton: UIButton!
    @IBOutlet weak var switchLoginRegisterButton: UIButton!
    
    var type:Login_or_Register!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //try? VideoBackground.shared.play(view: view, videoName: "loginVideo", videoType: "mp4")
        
        initViews()
        
        
    }
    
    func initViews(){
        googleButton.layer.cornerRadius = 10
        googleButton.backgroundColor = UIColor.grey17alpha0_8
        
        mailTextField.backgroundColor = UIColor.grey17alpha0_8
        mailTextField.isOpaque = true
        mailTextField.layer.masksToBounds = true
        passwordTextField.backgroundColor = UIColor.grey17alpha0_8
        passwordTextField.isOpaque = true
        passwordTextField.layer.masksToBounds = true
        
        loginRegisterButton.layer.cornerRadius = 10
        
        switch type {
            case .Login:
                loginRegisterButton.setTitle("Login", for: .normal)
                switchLoginRegisterButton.setTitle("Don’t you have an account?", for: .normal)
                break
            case .Register:
                loginRegisterButton.setTitle("Register", for: .normal)
                switchLoginRegisterButton.setTitle("Do you already have an account?", for: .normal)
                break
            case .none:
                break
        }
        
    }
    
    
    @IBAction func googleButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func loginRegisterPressed(_ sender: Any) {
        
    }
    
    @IBAction func switchLoginRegisterButton(_ sender: Any) {
        
    }
    


}
