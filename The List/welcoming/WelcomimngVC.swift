//
//  ViewController.swift
//  The List
//
//  Created by Orhun Dündar on 17.12.2019.
//  Copyright © 2019 Orhun Dündar. All rights reserved.
//

import UIKit
import SwiftVideoBackground

class WelcomimngVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var getStartedButton: UIButton!
    
    var frontVC:LoginRegisterVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        getStartedButton.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 10
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        try? VideoBackground.shared.play(view: view, videoName: "loginVideo", videoType: "mp4")
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frontVC.view.frame.origin.x = self.view.frame.maxX
            self.backButton.isHidden = true
        }) { (Bool) in
            self.frontVC.view.removeFromSuperview()
            self.frontVC = nil
            UIView.animate(withDuration: 0.2) {
                self.loginButton.isHidden = false
                self.getStartedButton.isHidden = false
            }
        }
        
    }
    
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Welcoming", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginRegisterVC") as? LoginRegisterVC
        vc?.view.frame = self.view.bounds
        vc?.type = .Login
        self.frontVC = vc
        bringView(topView: (vc?.view)!)
    }
    
    @IBAction func getStartedPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Welcoming", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginRegisterVC") as? LoginRegisterVC
        vc?.view.frame = self.view.bounds
        vc?.type = .Register
        self.frontVC = vc
        bringView(topView: (vc?.view)!)
    }
    
    func bringView(topView:UIView) {
        self.backButton.isHidden = false
        self.loginButton.isHidden = true
        self.getStartedButton.isHidden = true
        
        
        topView.frame = self.view.frame
        topView.frame.origin.x = self.view.frame.maxX
        self.view.addSubview(topView)
        
        frontVC.loginRegisterButton.addTarget(self, action: #selector(createOnboarding), for: .touchUpInside)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            topView.frame.origin.x = 0
        })
        view.bringSubviewToFront(backButton)
    }
    
    @objc func createOnboarding(){
        let vc = UIStoryboard.init(name: "Onboarding", bundle: Bundle.main).instantiateViewController(withIdentifier: "OnboardingVC") as? OnboardingVC
        vc?.modalPresentationStyle = .fullScreen
        present(vc!, animated: true)
    }

}

