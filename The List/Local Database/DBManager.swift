//
//  DBManager.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import RealmSwift
import ObjectMapper

class DBManager {
    
    static let currentSchemaVersion: UInt64 = 1
    
    ///Delete All Local Data
    public static func deleteAll(success: @escaping (Bool) -> Void){
        let mRealm = try! Realm()
        try! mRealm.write {
            mRealm.deleteAll()
            success(true)
        }
    }
    
    ///Configure Local Database
    static func configureMigration() {
        var config = Realm.Configuration(schemaVersion: currentSchemaVersion, migrationBlock: { (migration, oldSchemaVersion) in
            debugPrint("oldSchemaVersion: " + String(oldSchemaVersion))
        })
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
    }
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
}
