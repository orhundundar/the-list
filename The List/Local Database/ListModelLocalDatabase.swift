//
//  ListModelLocalDatabase.swift
//  The List
//
//  Created by Orhun Dündar on 4.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

class ListModelLocalDatabase {
    
    /// Save List To Local Database
    ///
    /// - Parameter  ListModel: ListModel
    public static func saveListModel(listModel:ListModel!, result: @escaping (String) -> Void) {
        let realm = try! Realm()
        let localListModels:Results<ListModelForLocal>! = realm.objects(ListModelForLocal.self).filter("_id = %@", listModel._id)
        
        if localListModels.count == 0
        {
            let listModelForLocal = ListModelForLocal(value: ["_id": listModel._id])
            listModelForLocal.setList(list: listModel)
            
            try! realm.write {
                realm.add(listModelForLocal)
            }
            result(listModelForLocal._id)
        }
        else
        {
            result(listModel._id)
        }
    }
    
    /// Get ListModel
    ///
    /// - Returns: ListModel
    public static func getListModel(_id:String, result: @escaping (ListModel?) -> Void) {
        let realm = try! Realm()
        let results: Results<ListModelForLocal> = realm.objects(ListModelForLocal.self).filter("_id = %@" , _id)
        if results.count > 0 {
            let listModel : ListModel = (results.first?.list)!
            result(listModel)
        }
        else {
            result(nil)
        }
    }
    
    /// Get ListModels
    ///
    /// - Returns: All ListModel
    public static func getListModels(result: @escaping ([ListModel]) -> Void) {
        let realm = try! Realm()
        let results: Results<ListModelForLocal> = realm.objects(ListModelForLocal.self)
        var listModelArray:[ListModel] = []
        for result in results {
            listModelArray.append(result.list!)
        }
        
        result(listModelArray)
    }
    
    /// Update ListModel
    ///
    /// - Parameters:
    ///   - listModel: updated listModel
    public static func updateListModel(listModel: ListModel, result: @escaping (Bool) -> Void)  {
        let realm = try! Realm()
        let listModels:Results<ListModelForLocal> = realm.objects(ListModelForLocal.self).filter("_id = %@" , listModel._id)
        let updatedlistModel:ListModel = listModel
        //updatedlistModel.dateModified = DateFormatTransform.createTestDate()
        
        if let listModel = listModels.first {
            try! realm.write {
                //listModel.isUpdate = true
                listModel.setList(list: updatedlistModel)
                result(true)
            }
        }else {
            result(false)
        }
    }
    
}
