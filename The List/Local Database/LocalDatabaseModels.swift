//
//  ListModel.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import RealmSwift
import ObjectMapper

public class ListModelForLocal: Object {
    @objc dynamic var _id:String = ObjectID.createObjectId()
    @objc dynamic var isSend: Bool = true   //GÖNDERİLECEK
    @objc dynamic var isUpdate: Bool = false    //GÜNCELLENECEK
    @objc dynamic var isDeleted: Bool = false
    @objc dynamic var sending: Bool = false
    @objc dynamic var strListModel: String = ""
    
    override public static func primaryKey() -> String? {
        return "_id"
    }
    
    public func setList(list:ListModel) {
        strListModel = Mapper().toJSONString(list, prettyPrint: true)!
    }
    
    var list:ListModel? {
        return Mapper<ListModel>().map(JSONString: strListModel)
    }
    
}
