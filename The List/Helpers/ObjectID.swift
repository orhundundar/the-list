//
//  ObjectID.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation

class ObjectID {
    public static func createObjectId() -> String {
        let timestamp = String(Int(Date().timeIntervalSince1970), radix: 16, uppercase: false)
        let randomValue = String(Int.random(in: 1000000000 ..< 9999999999))
        let counter = String(Int.random(in: 100000 ..< 999999))
        return timestamp + randomValue  + counter
    }
}
