//
//  UserDefaults.swift
//  The List
//
//  Created by Orhun Dündar on 14.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    public static func setUser(username:String?, password:String?) {
        UserDefaults.standard.set(username, forKey: "user_username")
        UserDefaults.standard.set(password, forKey: "user_password")
    }
    
    public static func getUser() -> SystemUser? {
        let username:String? = UserDefaults.standard.value(forKey: "user_username") as! String?
        let password:String? = UserDefaults.standard.value(forKey: "user_password") as! String?
        
        if username == nil || password == nil {
            return nil
        }
        else {
            let user: SystemUser = SystemUser(username: username!, password: password!)
            return user
        }
    }
    
    /// Show onboarding
    public static func setOnboardingShowed(isShowed:Bool?) {
        UserDefaults.standard.set(isShowed, forKey: "onboarding_showed")
    }
    
    public static func getOnboardingShowed() -> Bool? {
        let isShowed:Bool? = UserDefaults.standard.value(forKey: "onboarding_showed") as! Bool?
        if isShowed == nil {
            return false
        }
        return isShowed
    }
    
}

public class SystemUser{
    
    var username:String!
    var password:String!
    
    init(username:String,password:String) {
        self.username = username
        self.password = password
    }
}

