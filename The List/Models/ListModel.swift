//
//  ListModel.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import ObjectMapper
import MobileCoreServices

public class ListModel : NSObject, Mappable, NSItemProviderWriting, NSItemProviderReading {
    var image2D:Data?
    public static var readableTypeIdentifiersForItemProvider = [kUTTypeData as String]

    public static func object(withItemProviderData data: Data, typeIdentifier: String) throws -> Self {
        return try self.init(itemProviderData: data, typeidentifier: kUTTypeData as String)
    }

    required init(itemProviderData data: Data, typeidentifier: String) throws {
        super.init()
        image2D = data
    }

    public static var writableTypeIdentifiersForItemProvider = [kUTTypeData as String]

    public func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        let data2E = image2D
        completionHandler(data2E, nil)
        return nil
    }
    
    var _id:String = ObjectID.createObjectId()
    var name:String!
    var list:[ItemForList] = []
    var imageString:String!
    var imageLink:String!
    var doneList:[ItemForList] {
        var _list:[ItemForList] = []
        for item in list {
            if item.isDone {
                _list.append(item)
            }
        }
        return _list
    }
    var undoneList:[ItemForList] {
        var _list:[ItemForList] = []
        for item in list {
            if !item.isDone {
                _list.append(item)
            }
        }
        return _list
    }
    var listItemCount:Int {
        get {
            list.count
        }
    }
    var doneItemPercent:Int {
        get {
            return (doneList.count * 100)/list.count
        }
    }
    
    var procres:String {
        return "\(String(doneList.count))/\(String(list.count))"
    }
    
    override init() {}
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        _id <- map["_id"]
        name <- map["name"]
        list <- map["list"]
        imageString <- map["imageString"]
        imageLink <- map["imageLink"]
    }
}


public class ItemForList : Mappable {
    var _id:String = ObjectID.createObjectId()
    var type_id:Int!
    var name:String!
    var imageString:String?
    var imageLink:String?
    var isDone:Bool = false
    
    var book:BookItem!
    var movie:MovieItem!
    var experiance:ExperianceItem!
    
    var detail:String {
        switch type {
        case .Book:
            return book.writter
        case .Movie:
            return movie.year + " | " + movie.genre
        case .Experiance:
            return experiance.notes ?? ""
        }
    }
    
    var type:ItemType {
        return ItemType.getType(value: type_id)
    }
    
    init() {}
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        _id <- map["_id"]
        type_id <- map["type_id"]
        name <- map["name"]
        imageString <- map["imageString"]
        imageLink <- map["imageLink"]
        isDone <- map["isDone"]
        book <- map["book"]
        movie <- map["movie"]
        experiance <- map["experiance"]
        
    }
}
