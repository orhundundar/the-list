//
//  TMDBModels.swift
//  The List
//
//  Created by Orhun Dündar on 27.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import Foundation
import UIKit

class Movie: NSObject {
    private(set)var avgRating: Double?
    private(set)var title: String?
    private(set)var overview: String?
    private(set)var posterImageURLMedium: String?
    private(set)var posterImageURLHigh: String?
    private(set)var posterImageURLLow: String?
    private(set)var releaseYear: String?

    init(dictionary: NSDictionary) {
        let posterImagePath = dictionary["poster_path"] as? String
        let title = dictionary["title"] as? String
        let overview = dictionary["overview"] as? String
        //let releaseYear = (dictionary["release_date"] as? String)?.split(separator: "-").map {String($0)}[0]
        let avgRating = dictionary["vote_average"] as? Double
        self.title = title
        self.overview = overview
        //self.releaseYear = releaseYear
        self.avgRating = avgRating
        
        if let posterImagePath = posterImagePath {
            self.posterImageURLMedium = (TheMovieDBApi.imageBaseStr + "w500" + posterImagePath)
            self.posterImageURLHigh = (TheMovieDBApi.imageBaseStr + "original" + posterImagePath)
            self.posterImageURLLow =  (TheMovieDBApi.imageBaseStr + "w45" + posterImagePath)
        }
        
    }
    
    class func movies(with dictionaries: [NSDictionary]) -> [Movie] {
        return dictionaries.map {Movie(dictionary: $0)}
    }
    
    func toMovieItemForLocalDB() -> MovieItem {
        
        let movieItem = MovieItem()
        movieItem.name = title
        movieItem.plot = overview
        return movieItem
    }
    
}
