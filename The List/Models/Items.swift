//
//  Enums.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import ObjectMapper


public class BookItem : Mappable {
    var name:String!
    var isbn:String!
    var image:UIImage!
    var imageLink:String!
    var writter:String!
    var about:String!
    var year:String!
    
    init() {}
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        isbn <- map["isbn"]
        image <- map["image"]
        imageLink <- map["imageLink"]
        writter <- map["writter"]
        about <- map["about"]
        year <- map["year"]
    }
}


public class MovieItem : Mappable {
    var name:String!
    var image:UIImage!
    var year:String = ""
    var genre:String = ""
    var plot:String!
    var cast:String!
    var url:String!
    
    init() {}
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        image <- map["image"]
        year <- map["year"]
        genre <- map["genre"]
        plot <- map["plot"]
        cast <- map["cast"]
        url <- map["url"]
    }
}


public class ExperianceItem : Mappable {
    var name:String!
    var image:UIImage!
    var notes:String!
    
    init() {}
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        image <- map["image"]
        notes <- map["notes"]
    }
}
