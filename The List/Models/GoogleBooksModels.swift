//
//  GoogleBooksModels.swift
//  The List
//
//  Created by Orhun Dündar on 14.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import ObjectMapper


public class BooksForGoogleApi : Mappable {
    var id:String?
    var title:String?
    var authors:String?
    var desc:String?
    var imurl:String?
    var url:String?
    
    init (id: String, title: String, authors: String, desc: String, imurl: String, url: String) {
        self.id = id
        self.title = title
        self.authors = authors
        self.desc = desc
        self.imurl = imurl
        self.url = url
    }
    
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        authors <- map["authors"]
        desc <- map["desc"]
        imurl <- map["imurl"]
        url <- map["url"]
    }
}
