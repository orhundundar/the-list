//
//  OnboardingNameVC.swift
//  The List
//
//  Created by Orhun Dündar on 14.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class OnboardingNameVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainNavigationController")
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
}
