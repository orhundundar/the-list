//
//  OnboardingVC.swift
//  The List
//
//  Created by Orhun Dündar on 14.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class OnboardingVC: UIPageViewController {
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIdentifier: "OnboardingPageVC"),
            self.getViewController(withIdentifier: "OnboardingPageVC"),
            self.getViewController(withIdentifier: "OnboardingPageVC"),
            self.getViewController(withIdentifier: "OnboardingPageVC"),
            UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: "OnboardingNameVC")
        ]
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController {
        let vc = UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: identifier) as! OnboardingPageVC
        vc.setView(pageNumber: self.pageNumber)
        pageNumber = pageNumber + 1
        return vc
    }
    
    var pageControl = UIPageControl()
    
    var pageNumber = 0
    let skipButton = UIButton()
    let nextButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.delegate = self
        self.dataSource = self
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        for (i,page) in self.pages.enumerated() {
            if i < 4 {
                (page as! OnboardingPageVC).setView(pageNumber: i)
            }
        }
        
        UserDefaults.setOnboardingShowed(isShowed: true)
    }
    
    private func setupView() {
        let appearance = UIPageControl.appearance(whenContainedInInstancesOf: [UIPageViewController.self])
        appearance.pageIndicatorTintColor = UIColor(red: 0.894, green: 0.914, blue: 0.933, alpha: 1)
        appearance.currentPageIndicatorTintColor = UIColor(red: 0.161, green: 0.192, blue: 0.259, alpha: 1)
        self.view.backgroundColor = .white
        setupPageControl()
        
    }
    
    func setupPageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 75,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = pages.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.lightGray
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.pageControl.isUserInteractionEnabled = false
        pageControl.backgroundColor = UIColor.clear
        self.view.addSubview(pageControl)
    }
    
    
}

extension OnboardingVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0          else { return pages.last }
        guard pages.count > previousIndex else { return nil        }
        return pages[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count else { return pages.first }
        guard pages.count > nextIndex else { return nil         }
        return pages[nextIndex]
    }
    
    // MARK: Delegate functions
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = pages.firstIndex(of: pageContentViewController)!
        
    }
    
}


