//
//  OnboardingPageVC.swift
//  The List
//
//  Created by Orhun Dündar on 13.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class OnboardingPageVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var mainTitle:String!
    var subTitle:String!
    var image:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = mainTitle
        subTitleLabel.text = subTitle
        imageView.image = image
    }
    
    func setView(pageNumber:Int) {
        
        switch pageNumber {
        case 0:
            mainTitle = "Welcome to place where you can list "
            subTitle = "The List will help you to accomplish long term goals. Because, once you write it down, you 42% more likely to achieve it."
            image = UIImage(named: "onboarding1")
            break
        case 1:
            mainTitle = "Only the most important things"
            subTitle = "You know, life is short. So, this shouldn’t be an ordinary to do list. Books, movies, series and of course, experiences are suggested."
            image = UIImage(named: "onboarding2")
            break
        case 2:
            mainTitle = "Find easily what you’re looking for"
            subTitle = "We gather many resources in one place for you. Search for book names, directors, cities. You can add them to your list easily."
            image = UIImage(named: "onboarding3")
            break
        case 3:
            mainTitle = "Share your lists to others"
            subTitle = "You can send the lists you created with your friends and get lists from others. Sharing is caring."
            image = UIImage(named: "onboarding4")
            break
        default:
            break
        }
        
    }
    
}
