//
//  ItemListCell.swift
//  The List
//
//  Created by Orhun Dündar on 27.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class ItemListCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var isDoneViewSetted:Bool = false
    var transparentView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemImageView.backgroundColor = UIColor.grey1
        subTitleLabel.font = UIFont.Secondary1
        subTitleLabel.textColor = UIColor.grey1
    }
    
    func setItem(item:ItemForList) {
        self.itemImageView.image = PhotoManager.getSavedImage(named: item.imageString ?? nil)
        self.title.text = item.name
        self.subTitleLabel.text = item.detail
    }
    
    func makeViewDone(){
        if !isDoneViewSetted {
            let view = UIView()
            view.frame = self.bounds
            view.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            self.addSubview(view)
            isDoneViewSetted = true
            self.transparentView = view
        }
    }
    
    func removeTransparentView(){
        if transparentView != nil {
            transparentView.removeFromSuperview()
            transparentView = nil
            isDoneViewSetted = false
        }
    }
    
    @IBAction func moreButtonPressed(_ sender: Any) {
    }
    

}
