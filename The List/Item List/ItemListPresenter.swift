//
//  ItemListPresenter.swift
//  The List
//
//  Created by Orhun Dündar on 14.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import Foundation

protocol ItemListProtocol {
    func didReachedUndoneItemList(itemList:[ItemForList])
    func didReachedDoneItemList(itemList:[ItemForList])
    func didReachedListInfos(name:String,subInfo:String)
}

class ItemListPresenter {
    
    var mainDelegate:ItemListProtocol!
    var mainViewController:ItemListVC!
    
    var listModel:ListModel!
    
    init(delegate:ItemListProtocol, vc:ItemListVC, listModel:ListModel){
        self.mainDelegate = delegate
        self.mainViewController = vc
        self.listModel = listModel
    }
    
    func getDatas(){
        ListModelLocalDatabase.getListModel(_id: listModel._id) { (listModel) in
            self.listModel = listModel
            
            self.mainDelegate.didReachedListInfos(name: (listModel?.name)!, subInfo: listModel!.procres)
            self.mainDelegate.didReachedUndoneItemList(itemList: self.listModel.undoneList)
            self.mainDelegate.didReachedDoneItemList(itemList: self.listModel.doneList)
        }
    }
    
    func changeItemStatus(id:String, done:Bool){
        self.listModel.list.first(where: {$0._id == id})?.isDone = done
        ListModelLocalDatabase.updateListModel(listModel: self.listModel) { (Bool) in
            self.mainDelegate.didReachedListInfos(name: (self.listModel?.name)!, subInfo: self.listModel!.procres)
            self.mainDelegate.didReachedUndoneItemList(itemList: self.listModel.undoneList)
            self.mainDelegate.didReachedDoneItemList(itemList: self.listModel.doneList)
        }
    }
    
    
}
