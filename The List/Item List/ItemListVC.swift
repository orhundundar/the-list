//
//  ItemListVC.swift
//  The List
//
//  Created by Orhun Dündar on 26.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class ItemListVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var listModel:ListModel!
    var presenter:ItemListPresenter!
    var undoneList : [ItemForList] = []
    var doneList : [ItemForList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ItemListPresenter(delegate: self, vc: self, listModel: self.listModel)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.getDatas()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moreButtonPressed(_ sender: Any) {
    }
    
}

extension ItemListVC : ItemListProtocol {
    func didReachedDoneItemList(itemList: [ItemForList]) {
        self.doneList = itemList
        self.tableView.reloadData()
    }
    
    func didReachedUndoneItemList(itemList: [ItemForList]) {
        self.undoneList = itemList
        self.tableView.reloadData()
    }
    
    func didReachedListInfos(name: String, subInfo: String) {
        self.titleLabel.text = name
        self.subTitleLabel.text = subInfo
    }
}

extension ItemListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if doneList.count > 0 {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.undoneList.count
        case 2:
            return self.doneList.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addItemCell", for: indexPath)
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemListCell", for: indexPath) as! ItemListCell
            cell.setItem(item:undoneList[indexPath.row])
            cell.removeTransparentView()
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemListCell", for: indexPath) as! ItemListCell
            cell.setItem(item:doneList[indexPath.row])
            cell.makeViewDone()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc = UIStoryboard.init(name: "AddItem", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddItemVC") as? AddItemVC
            vc!.listModel = presenter.listModel
            self.navigationController!.pushViewController(vc!, animated: true)
        }
        else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 56
        }
        else {
            return 92
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 1 {
            let doneAction = UIContextualAction(style: .normal, title:  "Done", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                self.presenter.changeItemStatus(id:self.undoneList[indexPath.row]._id, done: true)
                    success(true)
             })
            doneAction.backgroundColor = .grey1
            
            return UISwipeActionsConfiguration(actions: [doneAction])
        }
        else if indexPath.section == 2 {
            let doneAction = UIContextualAction(style: .normal, title:  "Undone", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                self.presenter.changeItemStatus(id:self.doneList[indexPath.row]._id, done: false)
                    success(true)
             })
            doneAction.backgroundColor = .grey1
            
            return UISwipeActionsConfiguration(actions: [doneAction])
        }
        return UISwipeActionsConfiguration(actions: [])
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 32))
        let label = UILabel()
        label.frame = headerView.frame
        label.text = "COMPLETED"
        label.font = UIFont.ALLCAPS
        label.textColor = UIColor.grey4
        label.textAlignment = .center
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 32
        }
        return 0
    }
    
}
