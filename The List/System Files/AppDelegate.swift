//
//  AppDelegate.swift
//  The List
//
//  Created by Orhun Dündar on 17.12.2019.
//  Copyright © 2019 Orhun Dündar. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        let homeStoryBoard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let loginStoryBoard:UIStoryboard = UIStoryboard.init(name: "Welcoming", bundle: nil)
        
        let launchScreenStoryBoard:UIStoryboard = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let vc = launchScreenStoryBoard.instantiateViewController(withIdentifier: "LaunchScreen")
        self.window?.rootViewController = vc
        
        if UserDefaults.getUser() != nil {
            //App
            let vc1 = homeStoryBoard.instantiateViewController(withIdentifier: "MainNavigationController")
            self.window?.rootViewController = vc1
            
        }
        else {
            //LOGIN 
            let vc1 = loginStoryBoard.instantiateViewController(withIdentifier: "WelcomimngVC")
            self.window?.rootViewController = vc1
        }
        
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
}

