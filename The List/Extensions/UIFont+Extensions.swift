//
//  File.swift
//  The List
//
//  Created by Orhun Dündar on 10.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIFont {
    
    @nonobjc class var Large: UIFont {
        return UIFont(name: "NewYorkExtraLarge-Black", size: 34)!
    }
    
    @nonobjc class var Header1_Black: UIFont {
        return UIFont(name: "NewYorkLarge-Black", size: 26)!
    }
    
    @nonobjc class var Header1_Semibold: UIFont {
        return UIFont(name: "NewYorkLarge-Semibold", size: 26)!
    }
    
    @nonobjc class var Header2: UIFont {
        return UIFont(name: "NewYorkMedium-Semibold", size: 22)!
    }
    
    @nonobjc class var Header3: UIFont {
        return UIFont(name: "NewYorkMedium-Semibold", size: 18)!
    }
    
    @nonobjc class var Header4: UIFont {
        return UIFont(name: "NewYorkMedium-Semibold", size: 17)!
    }
    
    @nonobjc class var Button: UIFont {
        return UIFont.systemFont(ofSize: 16, weight: Weight.bold)
    }
    
    @nonobjc class var BodyBold: UIFont {
        return UIFont.systemFont(ofSize: 15, weight: Weight.bold)
    }
    
    @nonobjc class var BodySB: UIFont {
        return UIFont.systemFont(ofSize: 15, weight: Weight.semibold)
    }
    
    @nonobjc class var BodyRegular: UIFont {
        return UIFont.systemFont(ofSize: 15, weight: Weight.regular)
    }
    
    @nonobjc class var ALLCAPS: UIFont {
        return UIFont.systemFont(ofSize: 13, weight: Weight.regular)
    }
    
    @nonobjc class var SmallTextSemiBold: UIFont {
        return UIFont.systemFont(ofSize: 13, weight: Weight.semibold)
    }
    
    @nonobjc class var Secondary1: UIFont {
        return UIFont.systemFont(ofSize: 13, weight: Weight.regular)
    }
    
    @nonobjc class var Secondary2: UIFont {
        return UIFont.systemFont(ofSize: 12, weight: Weight.regular)
    }
    
    @nonobjc class var TabBarSideSelected: UIFont {
        return UIFont.systemFont(ofSize: 10, weight: Weight.semibold)
    }
    
    @nonobjc class var TabBarSide: UIFont {
        return UIFont.systemFont(ofSize: 10, weight: Weight.regular)
    }
    
}
