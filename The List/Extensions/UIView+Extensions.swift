//
//  UIView+Extensions.swift
//  The List
//
//  Created by Orhun Dündar on 19.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIView {
    
    func setGradiantFadeOutToBottom(yStartPoint:Int) {
        //Botttom Buttons view
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.5, y: (Double(self.frame.height) - Double(yStartPoint))/Double(self.frame.height))
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.colors = [UIColor.white.cgColor,UIColor.init(white: 1, alpha: 0).cgColor]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
