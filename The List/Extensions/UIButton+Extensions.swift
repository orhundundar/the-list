//
//  UIButton+Extensions.swift
//  The List
//
//  Created by Orhun Dündar on 3.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setBorderdButton(){
        self.layer.borderColor = UIColor.grey15.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 6
        
        self.setTitleColor(UIColor.black, for: .highlighted)
        self.setBackgroundColor(UIColor.grey16, for: .highlighted)
    }
    
}

extension UIButton {
    private func imageWithColor(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }

    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        self.setBackgroundImage(imageWithColor(color: color), for: state)
    }
}
