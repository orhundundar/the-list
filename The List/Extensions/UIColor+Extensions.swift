//
//  UIColor+Extensions.swift
//  The List
//
//  Created by Orhun Dündar on 10.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var  white: UIColor {
        return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @nonobjc class var  grey18: UIColor {
        return UIColor(red: 0.99, green: 0.99, blue: 0.99, alpha: 1)
    }
    
    @nonobjc class var  grey17: UIColor {
        return UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
    }
    
    @nonobjc class var  grey17alpha0_8: UIColor {
        return UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
    }
    
    @nonobjc class var  grey16: UIColor {
        return UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1)
    }
    
    @nonobjc class var  grey15: UIColor {
        return UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
    }
    
    @nonobjc class var  grey14: UIColor {
        return UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1)
    }
    
    @nonobjc class var  grey13: UIColor {
        return UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1)
    }
    
    @nonobjc class var  grey12: UIColor {
        return UIColor(red: 0.73, green: 0.73, blue: 0.73, alpha: 1)
    }
    
    @nonobjc class var  grey11: UIColor {
        return UIColor(red: 0.69, green: 0.69, blue: 0.69, alpha: 1)
    }
    
    @nonobjc class var  grey10: UIColor {
        return UIColor(red: 0.64, green: 0.64, blue: 0.64, alpha: 1)
    }
    
    @nonobjc class var  grey9: UIColor {
        return UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
    }
    
    @nonobjc class var  grey8: UIColor {
        return UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
    }
    
    @nonobjc class var  grey7: UIColor {
        return UIColor(red: 0.52, green: 0.52, blue: 0.52, alpha: 1)
    }
    
    @nonobjc class var  grey6: UIColor {
        return UIColor(red: 0.48, green: 0.48, blue: 0.48, alpha: 1)
    }
    
    @nonobjc class var  grey5: UIColor {
        return UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1)
    }
    
    @nonobjc class var  grey4: UIColor {
        return UIColor(red: 0.39, green: 0.39, blue: 0.39, alpha: 1)
    }
    
    @nonobjc class var  grey3: UIColor {
        return UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
    }
    
    @nonobjc class var  grey2: UIColor {
        return UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
    }
    
    @nonobjc class var  grey1: UIColor {
        return UIColor(red: 0.259, green: 0.259, blue: 0.259, alpha: 1)
    }
    
    
}

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
}

