//
//  HomePageVC.swift
//  The List
//
//  Created by Orhun Dündar on 14.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import CollectionViewWaterfallLayout

class HomePageVC: UIViewController {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var noListView: UIView!
    @IBOutlet weak var noListTitleLabel: UILabel!
    @IBOutlet weak var noListSubTitleLabel: UILabel!
    @IBOutlet weak var noListCreteListButton: UIButton!
    
    
    var listsArray:[ListModel] = []
    var presenter:HomePagePresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        presenter = HomePagePresenter(delegate:self)
        
        presenter.loadDatas()
        
    }
    
    func initViews(){
        titleLabel.font = UIFont.Large
        noListTitleLabel.font = UIFont.Header3
        noListTitleLabel.textColor = UIColor.grey1
        noListTitleLabel.text = "Everything starts with a dream"
        noListSubTitleLabel.font = UIFont.Secondary1
        noListSubTitleLabel.textColor = UIColor.grey4
        noListSubTitleLabel.text = "Let’s get one step closer to your dreams."
        noListCreteListButton.layer.cornerRadius = 10
        noListCreteListButton.setTitleColor(UIColor.grey4, for: .normal)
        noListCreteListButton.backgroundColor = UIColor.grey15
        noListCreteListButton.titleLabel?.font = UIFont.BodyBold
        noListCreteListButton.setTitle("Create your first list", for: .normal)
        
        noListView.isHidden = false
        collectionView.isHidden = true
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.headerInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        layout.headerHeight = 50
        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        collectionView.collectionViewLayout = layout
        
        collectionView.dragDelegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.dropDelegate = self
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuVC") as? MenuVC
        vc?.modalPresentationStyle = .overCurrentContext
        self.present(vc!, animated: false)
    }
    
    @IBAction func noListCreateListButtonPressed(_ sender: Any) {
        presenter.alertWithTextField(viewController: self) { result in
            print(result)
        }
    }
    
}

// MARK: - HomePageProtocol
extension HomePageVC : HomePageProtocol {
    func listsCreated(lists: [ListModel]) {
        if lists.count == 0 {
            noListView.isHidden = false
            collectionView.isHidden = true
        }
        else {
            noListView.isHidden = true
            collectionView.isHidden = false
            listsArray = lists
            collectionView.reloadData()
        }
    }
    
    
}

// MARK: - UICollectionViewDataSource
extension HomePageVC : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listsArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateListCollectionViewCell", for: indexPath) as! CreateListCollectionViewCell
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.setList(list: listsArray[indexPath.row-1])
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            presenter.alertWithTextField(viewController: self) { result in
                print(result)
            }
        }
        else {
            let vc = UIStoryboard.init(name: "ItemList", bundle: Bundle.main).instantiateViewController(withIdentifier: "ItemListVC") as? ItemListVC
            vc?.listModel = listsArray[indexPath.row-1]
            self.navigationController!.pushViewController(vc!, animated: true)
        }
    }
    
}

// MARK: - CollectionViewWaterfallLayoutDelegate
extension HomePageVC : CollectionViewWaterfallLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: 163, height: 73)
        }
        else {
            return CGSize(width: 163, height: 220)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> Float {
        return 0
    }
}

// MARK: - CollectionViewDragDelegate and CollectionViewDropDelegate
extension HomePageVC : UICollectionViewDragDelegate, UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if indexPath.row>0 {
            let provider = NSItemProvider(object: listsArray[indexPath.row-1])
            let dragItem = UIDragItem(itemProvider: provider)
            return [dragItem]
        }
        else {
            return []
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo
      session: UIDragSession, at indexPath: IndexPath, point: CGPoint) ->
        [UIDragItem] {
        
        let provider = NSItemProvider(object: listsArray[indexPath.row-1] as NSItemProviderWriting)
        let dragItem = UIDragItem(itemProvider: provider)
        return [dragItem]
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: ListModel.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        loadAndInsertItems(at: destinationIndexPath, with: coordinator)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
    }
    
    private func loadAndInsertItems(at destinationIndexPath: IndexPath, with coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath: IndexPath
        
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let section = collectionView.numberOfSections - 1
            let row = collectionView.numberOfItems(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
        
        coordinator.session.loadObjects(ofClass: ListModel.self) { items in
            guard let string = items as? [ListModel] else { return }
            
            var indexPaths = [IndexPath]()
            
            for (index, value) in string.enumerated() {
                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
                self.listsArray.insert(value, at: indexPath.row)
                indexPaths.append(indexPath)
            }
            self.collectionView.insertItems(at: indexPaths)
        }
    }
    
}
