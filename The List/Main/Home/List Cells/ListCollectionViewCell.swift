//
//  ListCollectionViewCell.swift
//  The List
//
//  Created by Orhun Dündar on 14.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var graidentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    private var isGradientSetted:Bool = false
    
    public func setList(list:ListModel) {
        
        if list.list.count>=1 {
            var imageList:[UIImage] = []
            for list in list.list {
                imageList.append(PhotoManager.getSavedImage(named: list.imageString)!)
            }
            
            self.coverImageView.image = PhotoManager.collageImage(rect: self.coverImageView.frame, images: imageList)
        }
        else {
            self.coverImageView.image = UIImage(named:list.imageString)
        }
        
        self.titleLabel.text = list.name
        self.countLabel.text = String(list.doneList.count) + "/" + String(list.listItemCount)
    }
    
    private func setGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor]
        //graidentView.layer.insertSublayer(gradientLayer, at: 0)
        graidentView.layer.addSublayer(gradientLayer)
        self.bringSubviewToFront(titleLabel)
        self.bringSubviewToFront(countLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 6
        
        self.backgroundColor = UIColor.grey1
        
        if !isGradientSetted {
            setGradient()
            isGradientSetted = true
        }
    }
    
    
}
