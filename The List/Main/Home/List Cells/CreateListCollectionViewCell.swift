//
//  CreateListCollectionViewCell.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class CreateListCollectionViewCell: UICollectionViewCell {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 6
    }
    
}
