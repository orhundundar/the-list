//
//  MenuVC.swift
//  The List
//
//  Created by Orhun Dündar on 11.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import UIKit

class MenuVC : UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var archiveButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var detailLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    func initViews(){
        
        profileButton.setImage(UIImage(named: "profile_icon"), for: .normal)
        profileButton.setTitle("Profile", for: .normal)
        profileButton.titleLabel?.font = UIFont.BodyRegular
        profileButton.setTitleColor(UIColor.grey1, for: .normal)
        
        archiveButton.setImage(UIImage(named: "archice_icon"), for: .normal)
        archiveButton.setTitle("Archieved Lists", for: .normal)
        archiveButton.titleLabel?.font = UIFont.BodyRegular
        archiveButton.setTitleColor(UIColor.grey1, for: .normal)
        
        helpButton.setImage(UIImage(named: "help_icon"), for: .normal)
        helpButton.setTitle("Help", for: .normal)
        helpButton.titleLabel?.font = UIFont.BodyRegular
        helpButton.setTitleColor(UIColor.grey1, for: .normal)
        
        reviewButton.setImage(UIImage(named: "rating_icon"), for: .normal)
        reviewButton.setTitle("Leave us a review ", for: .normal)
        reviewButton.titleLabel?.font = UIFont.BodyRegular
        reviewButton.setTitleColor(UIColor.grey1, for: .normal)
        
        logoutButton.setImage(UIImage(named: "logout_icon"), for: .normal)
        logoutButton.setTitle("Logout", for: .normal)
        logoutButton.titleLabel?.font = UIFont.BodyRegular
        logoutButton.setTitleColor(UIColor.grey1, for: .normal)
        
        detailLabel.text = "The List© v0.1"
        detailLabel.font = UIFont.Secondary2
        detailLabel.textColor = UIColor.grey10
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Present Animation
        self.contentView.frame.origin.x = self.contentView.frame.origin.x - self.contentView.frame.width
        let color = self.view.backgroundColor
        self.view.backgroundColor = .clear
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.frame.origin.x = self.contentView.frame.origin.x + self.contentView.frame.width
            self.view.backgroundColor = color
        })
    }
    
}

extension MenuVC {
    // Dismiss the popup when touch anywhere
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self.view)
            if currentPoint.x > self.contentView.frame.width {
                //Dismiss Animation
                UIView.animate(withDuration: 0.3, animations: {
                    self.contentView.frame.origin.x = self.contentView.frame.origin.x - self.contentView.frame.width
                    self.view.backgroundColor = .clear
                }) { (Bool) in
                    self.dismiss(animated: false, completion: nil)
                }
                
            }
            
        }
    }
}
