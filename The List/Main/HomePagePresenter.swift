//
//  HomePagePresenter.swift
//  The List
//
//  Created by Orhun Dündar on 17.08.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation
import UIKit

protocol HomePageProtocol {
    func listsCreated(lists:[ListModel])
}

class HomePagePresenter {
    
    var delegate:HomePageProtocol!
    var lists:[ListModel] = []
    
    init(delegate:HomePageVC) {
        self.delegate = delegate
    }
    
    public func loadDatas() {
        getLists()
    }
    
    private func getLists(){
        
        ListModelLocalDatabase.getListModels { (lists) in
            self.delegate.listsCreated(lists: lists)
        }
        
    }
    
    private func addList(name:String){
        let list = ListModel()
        list._id = ObjectID.createObjectId()
        list.name = name
        list.imageString = "emptyListPlaceholder" + String(Int.random(in: 1...6))
        
        ListModelLocalDatabase.saveListModel(listModel: list) { (String) in
            self.loadDatas()
        }
        
    }
    
    public func alertWithTextField(viewController:UIViewController, completion: @escaping ((String) -> Void) = { _ in }) {
        
        let alert = UIAlertController(title: "Give your list a name", message: nil, preferredStyle: .alert)
        alert.addTextField { newTextField in
            newTextField.placeholder = "Bucked list"
            newTextField.borderStyle = .roundedRect
            
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { _ in completion("") })
        alert.addAction(UIAlertAction(title: "Create", style: .default) { action in
            if
                let textFields = alert.textFields,
                let tf = textFields.first,
                let result = tf.text
            { self.addList(name: result) }
            else
            { self.addList(name: "Bucked list") }
        })
        
        
        viewController.present(alert, animated: true)
        
        for textfield: UIView in alert.textFields! {
            let container: UIView = textfield.superview!
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
           effectView.removeFromSuperview()
        }
    }
    
}
