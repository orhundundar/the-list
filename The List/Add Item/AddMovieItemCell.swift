//
//  AddMovieItemCell.swift
//  The List
//
//  Created by Orhun Dündar on 2.03.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import UIKit
import SDWebImage

class AddMovieItemCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var presenter:AddItemPresenter!
    var movieItem:Movie!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.Header4
        titleLabel.textColor = UIColor.grey1
        subTitleLabel.font = UIFont.Secondary1
        subTitleLabel.textColor = UIColor.grey1
        
    }
    
    func setCell(movieItem:Movie,presenter:AddItemPresenter) {
        self.presenter = presenter
        self.movieItem = movieItem
        self.titleLabel.text = movieItem.title
        self.coverImageView.sd_setImage(with: URL(string: movieItem.posterImageURLMedium ?? ""), placeholderImage: UIImage(named: "movie_placeholder"))
        self.subTitleLabel.text = movieItem.overview
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        presenter.addMovieItem(movieItem: self.movieItem)
        addButton.setImage(UIImage(named: "check"), for: .normal)
        addButton.isEnabled = false
    }

}
