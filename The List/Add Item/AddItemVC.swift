//
//  AddItemVC.swift
//  The List
//
//  Created by Orhun Dündar on 3.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import UnsplashPhotoPicker

class AddItemVC: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var itemTypeTitleLaabel: UILabel!
    @IBOutlet weak var experianceButton: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var movieButton: UIButton!
    
    @IBOutlet weak var textFieldTitleLabel: UILabel!
    @IBOutlet weak var itemNameTextField: UITextField!
    
    @IBOutlet weak var coverImageView: UIView!
    @IBOutlet weak var coverTitleLabel: UILabel!
    @IBOutlet weak var selectFromPhotosButton: UIButton!
    @IBOutlet weak var insertFromUnsplashButton: UIButton!
    @IBOutlet weak var searchFromUnsplashButton: UIButton!
    
    @IBOutlet weak var experianceCoverView: UIView!
    @IBOutlet weak var experianceCoverImageView: UIImageView!
    @IBOutlet weak var removeExpeerianceCoverButton: UIButton!
    
    @IBOutlet weak var booksAndMoviesTableView: UITableView!
    
    var presenter:AddItemPresenter!
    var listModel:ListModel!
    var itemType:ItemType = ItemType.Experiance
    var bookList:[BookItem] = []
    var movieList:[Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = AddItemPresenter(delegate:self, listModel:listModel, vc: self)
        initViews()
    }
    
    private func initViews(){
        navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: UIColor.grey1,
         NSAttributedString.Key.font: UIFont.Header1_Semibold]
        saveButton.isEnabled = false
        
        experianceButton.setTitle("Experiances", for: .normal)
        experianceButton.titleLabel?.font = UIFont.Secondary1
        experianceButton.setImage(UIImage(named: "experiance_unselected"), for: .normal)
        experianceButton.setImage(UIImage(named: "experiance_selected"), for: .selected)
        experianceButton.setTitleColor(UIColor.grey10, for: .normal)
        experianceButton.setTitleColor(UIColor.grey1, for: .selected)
        experianceButton.isSelected = true
        
        bookButton.setTitle("Books", for: .normal)
        bookButton.titleLabel?.font = UIFont.Secondary1
        bookButton.setImage(UIImage(named: "book_unselected"), for: .normal)
        bookButton.setImage(UIImage(named: "book_selected"), for: .selected)
        bookButton.setTitleColor(UIColor.grey10, for: .normal)
        bookButton.setTitleColor(UIColor.grey1, for: .selected)
        
        movieButton.setTitle("Movies/Series", for: .normal)
        movieButton.titleLabel?.font = UIFont.Secondary1
        movieButton.setImage(UIImage(named: "movie_unselected"), for: .normal)
        movieButton.setImage(UIImage(named: "movie_selected"), for: .selected)
        movieButton.setTitleColor(UIColor.grey10, for: .normal)
        movieButton.setTitleColor(UIColor.grey1, for: .selected)
        
        removeExpeerianceCoverButton.setTitle("Remove", for: .normal)
        removeExpeerianceCoverButton.titleLabel?.font = UIFont.BodyBold
        removeExpeerianceCoverButton.setTitleColor(UIColor.grey6, for: .normal)
        
        booksAndMoviesTableView.isHidden = true
        coverImageView.isHidden = false
        experianceCoverView.isHidden = true
        
        selectFromPhotosButton.setBorderdButton()
        insertFromUnsplashButton.setBorderdButton()
        searchFromUnsplashButton.setBorderdButton()
    }
    
    private func setButtonUnselecteed(item:ItemType){
        self.itemType = item
        switch item {
        case .Experiance:
            experianceButton.isSelected=true
            bookButton.isSelected=false
            movieButton.isSelected=false
            break
        case .Book:
            experianceButton.isSelected=false
            bookButton.isSelected=true
            movieButton.isSelected=false
            break
        case .Movie:
            experianceButton.isSelected=false
            bookButton.isSelected=false
            movieButton.isSelected=true
            break
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        switch self.itemType {
        case .Experiance:
            presenter.addExperianceItem(name: itemNameTextField.text!)
            break
        case .Book:
            self.navigationController?.popViewController(animated: true)
            break
        case .Movie:
            break
        }
    }
    
    @IBAction func experianceButtonPressed(_ sender: Any) {
        setButtonUnselecteed(item: .Experiance)
        UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.coverImageView.isHidden = false
            self.booksAndMoviesTableView.isHidden = true
            self.textFieldTitleLabel.text = "WHAT DO YOU WANT TO ACHIEVE?"
            self.itemNameTextField.placeholder = "Climb Mt. Everest"
        })
    }
    
    @IBAction func bookButtonPressed(_ sender: Any) {
        setButtonUnselecteed(item: .Book)
        UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.coverImageView.isHidden = true
            self.booksAndMoviesTableView.isHidden = false
            self.textFieldTitleLabel.text = "BOOK INFO"
            self.itemNameTextField.placeholder = "Name, author or ISBN number"
        })
    }
    
    @IBAction func movieButtonPressed(_ sender: Any) {
        setButtonUnselecteed(item: .Movie)
        UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.coverImageView.isHidden = true
            self.booksAndMoviesTableView.isHidden = false
            self.textFieldTitleLabel.text = "MOVIE INFO"
            self.itemNameTextField.placeholder = "Name or genre"
        })
    }
    
    @IBAction func selectFromPhotosPressed(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image in
            self.presenter.photoSelectedFromGallery(image: image)
            self.experianceCoverImageView.image = image
            self.experianceCoverView.isHidden = false
        }
    }
    
    @IBAction func insertRandomPressed(_ sender: Any) {
        let photoManager = PhotoManager(sender: self)
        photoManager.insetRandomWithText(text:self.itemNameTextField.text)
    }
    
    @IBAction func searchInUnsplashPressed(_ sender: Any) {
        let photoManager = PhotoManager(sender: self)
        photoManager.showUnsplashView()
    }
    
    @IBAction func removeExpeerianceCoverButtonPressed(_ sender: Any) {
        UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.experianceCoverImageView.image = nil
            self.experianceCoverView.isHidden = true
            self.presenter.removeExpeerianceCover()
        })
    }
    
}

extension AddItemVC : AddItemProtocol , TheMovieDBDelegate {
    func didBooksReached(items: [BookItem]) {
        self.bookList = items
        DispatchQueue.main.async {
            self.booksAndMoviesTableView.reloadData()
        }
    }
    
    func itemAdded() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func theMovieDB(didFinishUpdatingMovies movies: [Movie]) {
        self.movieList = movies
        DispatchQueue.main.async {
            self.booksAndMoviesTableView.reloadData()
        }
    }
}

extension AddItemVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text != "" || textField.text != nil {
            if self.itemType == .Experiance {
                saveButton.isEnabled = true
            }
            else if self.itemType == .Book {
                self.presenter.getBooks(text:textField.text!)
            }
            else if self.itemType == .Movie {
                self.presenter.getMovies(text:textField.text!)
            }
        }
        return true
    }
}

extension AddItemVC : UnsplashPhotoPickerDelegate {
    func unsplashPhotoPicker(_ photoPicker: UnsplashPhotoPicker, didSelectPhotos photos: [UnsplashPhoto]) {
        
        let unsplashPhoto = photos.first!
        presenter.downloadImage(urlString: unsplashPhoto.urls[.small]!.absoluteString)
        self.experianceCoverView.isHidden = false
        
        let url = NSURL(string: unsplashPhoto.urls[.small]!.absoluteString)! as URL
        if let imageData: NSData = NSData(contentsOf: url) {
            let image = UIImage(data: imageData as Data)
            self.experianceCoverImageView.image = image
        }
        
    }
    
    func unsplashPhotoPickerDidCancel(_ photoPicker: UnsplashPhotoPicker) {
        
    }
    
}

extension AddItemVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension AddItemVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.itemType == .Book {
            return bookList.count
        }
        else {
            return movieList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.itemType == .Book {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddBookItemCell", for: indexPath) as! AddBookItemCell
            cell.setCell(bookItem: self.bookList[indexPath.row], presenter: self.presenter)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMovieItemCell", for: indexPath) as! AddMovieItemCell
            cell.setCell(movieItem: self.movieList[indexPath.row], presenter: self.presenter)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.itemType == .Book {
            return 92
        }
        else {
            return 92
        }
    }
    
    
}


