//
//  AddBookItemCell.swift
//  The List
//
//  Created by Orhun Dündar on 14.02.2021.
//  Copyright © 2021 Orhun Dündar. All rights reserved.
//

import UIKit
import SDWebImage

class AddBookItemCell: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var presenter:AddItemPresenter!
    var bookItem:BookItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.Header4
        titleLabel.textColor = UIColor.grey1
        subTitleLabel.font = UIFont.Secondary1
        subTitleLabel.textColor = UIColor.grey1
        
    }
    
    func setCell(bookItem:BookItem,presenter:AddItemPresenter) {
        self.presenter = presenter
        self.bookItem = bookItem
        self.titleLabel.text = bookItem.name
        self.coverImageView.sd_setImage(with: URL(string: bookItem.imageLink), placeholderImage: UIImage(named: "book_placeholder"))
        self.subTitleLabel.text = bookItem.about
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        presenter.addBookItem(book: self.bookItem)
        addButton.setImage(UIImage(named: "check"), for: .normal)
        addButton.isEnabled = false
    }
    
}
