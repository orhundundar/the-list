//
//  AddItemPresenter.swift
//  The List
//
//  Created by Orhun Dündar on 3.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import ObjectMapper

protocol AddItemProtocol {
    func itemAdded()
    func didBooksReached(items:[BookItem])
}

class AddItemPresenter {
    
    var delegate:AddItemProtocol!
    var listModel:ListModel!
    var vc:AddItemVC!
    
    let session = URLSession.shared
    
    var item:ItemForList = ItemForList()
    
    var imageURLForExperianceCover:String?
    var imageIDForExperianceCover:String?
    var imageForExperianceCover:UIImage?
    
    init(delegate:AddItemProtocol,listModel:ListModel,vc:AddItemVC) {
        self.delegate = delegate
        self.listModel = listModel
        self.vc = vc
    }
    
    public func addExperianceItem(name:String){
        
        let experiance = ExperianceItem()
        experiance.name = name
        experiance.image = imageForExperianceCover
        item.type_id = ItemType.Experiance.rawValue
        item.experiance = experiance
        self.item.imageString = imageIDForExperianceCover
        self.item.imageLink = imageURLForExperianceCover
        if imageForExperianceCover != nil && imageIDForExperianceCover != nil {
            PhotoManager.saveImage(image: imageForExperianceCover!, fileName: imageIDForExperianceCover!)
        }
        item.name = name
        listModel.list.append(item)
        ListModelLocalDatabase.updateListModel(listModel: self.listModel) { (Bool) in
            self.delegate.itemAdded()
        }
        
    }
    
    public func downloadImage(urlString:String) {
        var image: UIImage?
        let url = NSURL(string: urlString)! as URL
        if let imageData: NSData = NSData(contentsOf: url) {
            image = UIImage(data: imageData as Data)
            let imageName = ObjectID.createObjectId()
            imageIDForExperianceCover = imageName
            imageURLForExperianceCover = urlString
            imageForExperianceCover = image
        }
    }
    
    public func photoSelectedFromGallery(image:UIImage) {
        let imageName = ObjectID.createObjectId()
        imageIDForExperianceCover = imageName
        imageForExperianceCover = image
    }
    
    public func removeExpeerianceCover(){
        imageURLForExperianceCover = nil
        imageIDForExperianceCover = nil
        imageForExperianceCover = nil
    }
    
    public func getBooks(text:String) {
        GoogleBooksApi.getWith(text: text) { (bookList) in
            
            var list : [BookItem] = []
            
            for book in bookList! {
                let bookItem = BookItem()
                bookItem.name = book.title
                bookItem.writter = book.authors
                bookItem.about = book.desc
                bookItem.imageLink = book.imurl
                list.append(bookItem)
            }
            self.delegate.didBooksReached(items: list)
        }
    }
    
    public func addBookItem(book:BookItem){
        downloadImage(urlString:book.imageLink)
        item.type_id = ItemType.Book.rawValue
        item.book = book
        item.name = book.name
        self.item.imageString = imageIDForExperianceCover
        self.item.imageLink = imageURLForExperianceCover
        if imageForExperianceCover != nil && imageIDForExperianceCover != nil {
            PhotoManager.saveImage(image: imageForExperianceCover!, fileName: imageIDForExperianceCover!)
        }
        
        listModel.list.append(item)
        ListModelLocalDatabase.updateListModel(listModel: self.listModel) { (Bool) in
            self.delegate.itemAdded()
        }
        
    }
    
    
    public func getMovies(text:String) {
        
        let movieDB = TheMovieDBApi(endpoint: text)
        movieDB.delegate = self.vc
        movieDB.startUpdatingMovies()
    }
    
    public func addMovieItem(movieItem:Movie){
        downloadImage(urlString:movieItem.posterImageURLMedium!)
        item.type_id = ItemType.Movie.rawValue
        item.movie = movieItem.toMovieItemForLocalDB()
        item.name = movieItem.title
        self.item.imageString = imageIDForExperianceCover
        self.item.imageLink = imageURLForExperianceCover
        if imageForExperianceCover != nil && imageIDForExperianceCover != nil {
            PhotoManager.saveImage(image: imageForExperianceCover!, fileName: imageIDForExperianceCover!)
        }
        
        listModel.list.append(item)
        ListModelLocalDatabase.updateListModel(listModel: self.listModel) { (Bool) in
            self.delegate.itemAdded()
        }
        
    }
    
}

